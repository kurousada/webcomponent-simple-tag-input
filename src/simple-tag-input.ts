import "./simple-tag-input-tag";
import { LitElement, css, customElement, html, property } from "lit-element";
import { StringSetConverter } from "./set-converter";
import uniq from "./uniq";

const createUniqID = uniq("simple-tag-input");

@customElement("simple-tag-input")
export class SimpleTagInput extends LitElement {
  @property({ reflect: true, converter: StringSetConverter })
  tags: Set<string> = new Set();
  @property({ reflect: true, converter: StringSetConverter })
  value: Set<string> = new Set();
  @property({ type: Boolean })
  autocomplete = true;
  @property({ type: String })
  placeholder = "";
  @property({ type: Boolean })
  autofocus = false;
  @property({ type: Boolean })
  dismissible = true;
  @property({ type: Array })
  triggers = ["enter", " "];

  private uniqID: string;
  private isComposed = false; // flag to detect enter just after the composition
  private tagDeletableByBackspace = false;

  constructor() {
    super();
    this.uniqID = createUniqID();
  }

  static get styles() {
    return css`
      :host {
        display: flex;
        flex-wrap: wrap;
        border: var(--simple-tag-input-border, 1px solid gray);
        border-radius: var(--simple-tag-input-border-radius, 5px);
        padding: 0 5px 5px 5px;
        align-items: center;
        background: var(--simple-tag-input-background, transparent);
      }
      input {
        flex: 1;
        min-width: var(--simple-tag-input-input-min-width, 2rem);
        box-sizing: border-box;
        border: none;
        outline: none;
        background: transparent;
        color: var(--simple-tag-input-color, black);
        appearance: none;
        -ms-appearance: none;
        -moz-appearance: none;
        -webkit-appearance: none;
        font-family: inherit;
        font-size: 100%;
        line-height: 1.5rem;
        margin-top: 5px;
      }
    `;
  }

  firstUpdated() {
    if (this.autofocus) {
      this.shadowRoot?.querySelector("input")?.focus();
    }
  }

  render() {
    return html`
      ${[...this.value].map(
        (tag) => html`
          <simple-tag-input-tag
            part="tag"
            ?dismissible="${this.dismissible}"
            @dismiss="${this.onDismissed(tag)}"
            >${tag}</simple-tag-input-tag
          >
        `
      )}
      <input
        part="input"
        type="text"
        placeholder="${this.placeholder}"
        autocomplete="${this.autocomplete ? "on" : "off"}"
        ?autofocus="${this.autofocus}"
        list="${this.uniqID}-datalist"
        @keyup="${this.onKeyUp}"
        @compositionend="${this.onCompositionEnd}"
      />
      <datalist id="${this.uniqID}-datalist">
        ${[...this.tags].map(
          (tag) => html`<option value="${tag}">${tag}</option>`
        )}
      </datalist>
    `;
  }

  onDismissed(tag: string) {
    return () => {
      this.value.delete(tag);
      this.dispatchEvent(
        new CustomEvent("dismissed", { detail: { tags: [tag] } })
      );
      this.requestUpdate();
    };
  }

  onCompositionEnd() {
    this.isComposed = true;
  }

  onKeyUp(ev: Event) {
    const event = ev as KeyboardEvent;
    if (event.isComposing) {
      return;
    }
    const key = event.key.toLowerCase();
    const input = ev.target as HTMLInputElement;
    if (key === "enter" && this.isComposed) {
      // do nothing for enter just after the composition
      this.isComposed = false;
      this.tagDeletableByBackspace = input.selectionStart === 0;
      return;
    }

    const tags = input.value.split(/\s/).filter((tag) => tag !== "");
    if (this.triggers.includes(key) && tags.length > 0) {
      this.tags = new Set([...this.tags, ...tags]);
      this.value = new Set([...this.value, ...tags]);
      this.dispatchEvent(
        new CustomEvent("added", {
          detail: { inputValue: input.value, tags },
        })
      );
      input.value = "";
      this.requestUpdate();
    } else if (
      this.dismissible &&
      key === "backspace" &&
      this.tagDeletableByBackspace
    ) {
      const last = [...this.value][this.value.size - 1];
      this.value.delete(last); // remove last element
      this.dispatchEvent(
        new CustomEvent("dismissed", { detail: { tags: [last] } })
      );
      this.requestUpdate();
    }
    this.tagDeletableByBackspace = input.selectionStart === 0;
  }
}
