import { LitElement, css, customElement, html, property } from "lit-element";

@customElement("simple-tag-input-tag")
export class SimpleTagInputTag extends LitElement {
  @property({ type: Boolean })
  dismissible = false;

  static get styles() {
    return css`
      :host {
        color: var(--simple-tag-input-tag-color, black);
        background: var(--simple-tag-input-tag-background, #eee);
        border: var(--simple-tag-input-tag-border, 1px solid #999);
        border-radius: var(--simple-tag-input-tag-border-radius, 5px);
        display: flex;
        justify-content: space-between;
        align-items: center;
        white-space: nowrap;
        padding: 0 0 0 5px;
        margin-right: 5px;
        margin-top: 5px;
        height: 1.5rem;
        font-size: 0.9rem;
        width: min-content;
        cursor: default;
      }
      .dismissible {
        border: none;
        outline: none;
        color: var(--simple-tag-input-tag-dissmissable-color, black);
        background: var(
          --simple-tag-input-tag-dissmissable-background,
          transparent
        );
        padding: 0 5px;
        cursor: pointer;
      }
    `;
  }
  render() {
    return html`
      <slot></slot>
      ${this.dismissible
        ? html`<button
            class="dismissible"
            part="dismissible"
            @click="${this.onClicked}"
          >
            &times;
          </button>`
        : ""}
    `;
  }
  onClicked() {
    this.dispatchEvent(new CustomEvent("dismiss"));
  }
}
