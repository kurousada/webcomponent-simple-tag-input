export default function uniq(prefix: string): () => string {
  let counter = 0;
  return () => {
    counter++;
    return prefix + counter;
  };
}
