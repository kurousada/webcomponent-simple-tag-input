import { ComplexAttributeConverter } from "lit-element";

export type SetConverter<T> = ComplexAttributeConverter<Set<T>>;

export const StringSetConverter: SetConverter<string> = {
  fromAttribute(value: string | null) {
    if (value == null) {
      return new Set();
    }
    const obj = JSON.parse(value) as unknown;
    if (obj == null || !Array.isArray(obj)) {
      return new Set();
    }
    return new Set(...obj);
  },
  toAttribute(value: Set<string>) {
    return JSON.stringify([...value]);
  },
};
