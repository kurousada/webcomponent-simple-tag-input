var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import "./simple-tag-input-tag";
import { LitElement, css, customElement, html, property } from "lit-element";
import { StringSetConverter } from "./set-converter";
import uniq from "./uniq";
const createUniqID = uniq("simple-tag-input");
let SimpleTagInput = class SimpleTagInput extends LitElement {
    constructor() {
        super();
        this.tags = new Set();
        this.value = new Set();
        this.autocomplete = true;
        this.placeholder = "";
        this.autofocus = false;
        this.dismissible = true;
        this.triggers = ["enter", " "];
        this.isComposed = false;
        this.tagDeletableByBackspace = false;
        this.uniqID = createUniqID();
    }
    static get styles() {
        return css `
      :host {
        display: flex;
        flex-wrap: wrap;
        border: var(--simple-tag-input-border, 1px solid gray);
        border-radius: var(--simple-tag-input-border-radius, 5px);
        padding: 0 5px 5px 5px;
        align-items: center;
        background: var(--simple-tag-input-background, transparent);
      }
      input {
        flex: 1;
        min-width: var(--simple-tag-input-input-min-width, 2rem);
        box-sizing: border-box;
        border: none;
        outline: none;
        background: transparent;
        color: var(--simple-tag-input-color, black);
        appearance: none;
        -ms-appearance: none;
        -moz-appearance: none;
        -webkit-appearance: none;
        font-family: inherit;
        font-size: 100%;
        line-height: 1.5rem;
        margin-top: 5px;
      }
    `;
    }
    firstUpdated() {
        var _a, _b;
        if (this.autofocus) {
            (_b = (_a = this.shadowRoot) === null || _a === void 0 ? void 0 : _a.querySelector("input")) === null || _b === void 0 ? void 0 : _b.focus();
        }
    }
    render() {
        return html `
      ${[...this.value].map((tag) => html `
          <simple-tag-input-tag
            part="tag"
            ?dismissible="${this.dismissible}"
            @dismiss="${this.onDismissed(tag)}"
            >${tag}</simple-tag-input-tag
          >
        `)}
      <input
        part="input"
        type="text"
        placeholder="${this.placeholder}"
        autocomplete="${this.autocomplete ? "on" : "off"}"
        ?autofocus="${this.autofocus}"
        list="${this.uniqID}-datalist"
        @keyup="${this.onKeyUp}"
        @compositionend="${this.onCompositionEnd}"
      />
      <datalist id="${this.uniqID}-datalist">
        ${[...this.tags].map((tag) => html `<option value="${tag}">${tag}</option>`)}
      </datalist>
    `;
    }
    onDismissed(tag) {
        return () => {
            this.value.delete(tag);
            this.dispatchEvent(new CustomEvent("dismissed", { detail: { tags: [tag] } }));
            this.requestUpdate();
        };
    }
    onCompositionEnd() {
        this.isComposed = true;
    }
    onKeyUp(ev) {
        const event = ev;
        if (event.isComposing) {
            return;
        }
        const key = event.key.toLowerCase();
        const input = ev.target;
        if (key === "enter" && this.isComposed) {
            this.isComposed = false;
            this.tagDeletableByBackspace = input.selectionStart === 0;
            return;
        }
        const tags = input.value.split(/\s/).filter((tag) => tag !== "");
        if (this.triggers.includes(key) && tags.length > 0) {
            this.tags = new Set([...this.tags, ...tags]);
            this.value = new Set([...this.value, ...tags]);
            this.dispatchEvent(new CustomEvent("added", {
                detail: { inputValue: input.value, tags },
            }));
            input.value = "";
            this.requestUpdate();
        }
        else if (this.dismissible &&
            key === "backspace" &&
            this.tagDeletableByBackspace) {
            const last = [...this.value][this.value.size - 1];
            this.value.delete(last);
            this.dispatchEvent(new CustomEvent("dismissed", { detail: { tags: [last] } }));
            this.requestUpdate();
        }
        this.tagDeletableByBackspace = input.selectionStart === 0;
    }
};
__decorate([
    property({ reflect: true, converter: StringSetConverter })
], SimpleTagInput.prototype, "tags", void 0);
__decorate([
    property({ reflect: true, converter: StringSetConverter })
], SimpleTagInput.prototype, "value", void 0);
__decorate([
    property({ type: Boolean })
], SimpleTagInput.prototype, "autocomplete", void 0);
__decorate([
    property({ type: String })
], SimpleTagInput.prototype, "placeholder", void 0);
__decorate([
    property({ type: Boolean })
], SimpleTagInput.prototype, "autofocus", void 0);
__decorate([
    property({ type: Boolean })
], SimpleTagInput.prototype, "dismissible", void 0);
__decorate([
    property({ type: Array })
], SimpleTagInput.prototype, "triggers", void 0);
SimpleTagInput = __decorate([
    customElement("simple-tag-input")
], SimpleTagInput);
export { SimpleTagInput };
//# sourceMappingURL=simple-tag-input.js.map