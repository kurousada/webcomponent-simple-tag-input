export default function uniq(prefix: string): () => string;
