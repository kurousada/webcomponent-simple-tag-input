import "./simple-tag-input-tag";
import { LitElement } from "lit-element";
export declare class SimpleTagInput extends LitElement {
    tags: Set<string>;
    value: Set<string>;
    autocomplete: boolean;
    placeholder: string;
    autofocus: boolean;
    dismissible: boolean;
    triggers: string[];
    private uniqID;
    private isComposed;
    private tagDeletableByBackspace;
    constructor();
    static get styles(): import("lit-element").CSSResult;
    firstUpdated(): void;
    render(): import("lit-element").TemplateResult;
    onDismissed(tag: string): () => void;
    onCompositionEnd(): void;
    onKeyUp(ev: Event): void;
}
