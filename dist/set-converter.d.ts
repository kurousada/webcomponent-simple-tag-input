import { ComplexAttributeConverter } from "lit-element";
export declare type SetConverter<T> = ComplexAttributeConverter<Set<T>>;
export declare const StringSetConverter: SetConverter<string>;
