var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { LitElement, css, customElement, html, property } from "lit-element";
let SimpleTagInputTag = class SimpleTagInputTag extends LitElement {
    constructor() {
        super(...arguments);
        this.dismissible = false;
    }
    static get styles() {
        return css `
      :host {
        color: var(--simple-tag-input-tag-color, black);
        background: var(--simple-tag-input-tag-background, #eee);
        border: var(--simple-tag-input-tag-border, 1px solid #999);
        border-radius: var(--simple-tag-input-tag-border-radius, 5px);
        display: flex;
        justify-content: space-between;
        align-items: center;
        white-space: nowrap;
        padding: 0 0 0 5px;
        margin-right: 5px;
        margin-top: 5px;
        height: 1.5rem;
        font-size: 0.9rem;
        width: min-content;
        cursor: default;
      }
      .dismissible {
        border: none;
        outline: none;
        color: var(--simple-tag-input-tag-dissmissable-color, black);
        background: var(
          --simple-tag-input-tag-dissmissable-background,
          transparent
        );
        padding: 0 5px;
        cursor: pointer;
      }
    `;
    }
    render() {
        return html `
      <slot></slot>
      ${this.dismissible
            ? html `<button
            class="dismissible"
            part="dismissible"
            @click="${this.onClicked}"
          >
            &times;
          </button>`
            : ""}
    `;
    }
    onClicked() {
        this.dispatchEvent(new CustomEvent("dismiss"));
    }
};
__decorate([
    property({ type: Boolean })
], SimpleTagInputTag.prototype, "dismissible", void 0);
SimpleTagInputTag = __decorate([
    customElement("simple-tag-input-tag")
], SimpleTagInputTag);
export { SimpleTagInputTag };
//# sourceMappingURL=simple-tag-input-tag.js.map