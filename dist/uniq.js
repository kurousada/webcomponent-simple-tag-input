export default function uniq(prefix) {
    let counter = 0;
    return () => {
        counter++;
        return prefix + counter;
    };
}
//# sourceMappingURL=uniq.js.map