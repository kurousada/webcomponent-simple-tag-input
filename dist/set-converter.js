export const StringSetConverter = {
    fromAttribute(value) {
        if (value == null) {
            return new Set();
        }
        const obj = JSON.parse(value);
        if (obj == null || !Array.isArray(obj)) {
            return new Set();
        }
        return new Set(...obj);
    },
    toAttribute(value) {
        return JSON.stringify([...value]);
    },
};
//# sourceMappingURL=set-converter.js.map