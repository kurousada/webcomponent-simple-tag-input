import { LitElement } from "lit-element";
export declare class SimpleTagInputTag extends LitElement {
    dismissible: boolean;
    static get styles(): import("lit-element").CSSResult;
    render(): import("lit-element").TemplateResult;
    onClicked(): void;
}
