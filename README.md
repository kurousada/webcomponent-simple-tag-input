# Simple tag input webcomponent

Simple yet enough input component for tag editing.

![screenshot](https://gitlab.com/kurousada/webcomponent-simple-tag-input/-/raw/images/screenshot.gif)

## Features

 - Enter and space to add tags (configurable)
 - Autocomplete
 - Tags are dismissible (optional)
 - Backspace to remove last tag
 - Smart shrinking input area with flexbox
 - Customize styles in various ways
 - Developed in TypeScript and available in JavaScript & TypeScript
 - MIT License

...and more!

## Install

```sh
npm install simple-tag-input
```

## Example

<!--
```
<custom-element-demo>
  <template>
    <script type="module" src="dist/index.js"></script>
    <next-code-block></next-code-block>
  </template>
</custom-element-demo>
```
-->
```html
<simple-tag-input id="tagInput" placeholder="Enter your tags..."></simple-tag-input>
```

## Usage

### simple-tag-input

`simple-tag-input` is a custom element representing a container.
This holds input and `simple-tag-input-tag` elements as its children.

#### Example

```html
<simple-tag-input placeholder="Enter tags..." autofocus></simple-tag-input>
```

Or, visit https://kurousada.gitlab.io/webcomponent-simple-tag-input/

#### Properties

Name           | Default        | Description
---------------|----------------|-------------
`autocomplete` | true           | Whether enable autocomplete
`autofocus`    | false          | Whether the input get focus after load
`dismissible`  | true           | Whether tags are dismissible or not
`placeholder`  | empty string   | Placeholder of input
`tags`         | empty set      | Tags entered ever (including dismissed ones), used for autocomplete
`triggers`     | ["enter", " "] | Array of [KeyboardEvent#key](https://developer.mozilla.org/ja/docs/Web/API/KeyboardEvent/key) used for tag separation triggers
`value`        | empty set      | Tags entered

#### Events

##### added

Dispatched after some tags are added.

###### CustomEvent#detail

Name   | Type     | Description
-------|----------|-------------
`tags` | string[] | Tags added

##### dismissed

Dispatched after some tags are dismissed.

###### CustomEvent#detail

Name   | Type     | Description
-------|----------|-------------
`tags` | string[] | Tags dismissed

#### Styling

Styles specified to `simple-tag-input` by you will override default styles.

##### CSS Parts

We have `::part` ([Documentation](https://github.com/fergald/docs/blob/master/explainers/css-shadow-parts-1.md)) for custom styling.

Name    | Description
--------|-------------
`input` | `input` element inside the component
`tag`   | `simple-tag-input-tag` element inside the component

##### CSS Variables

For easy customization, we provide CSS variables.

Name                                 | Default          | Description
-------------------------------------|------------------|-------------
`--simple-tag-input-background`      | `transparent`    | Background of component
`--simple-tag-input-color`           | `black`          | Text color of input
`--simple-tag-input-border`          | `1px solid gray` | Border of component
`--simple-tag-input-border-radius`   | `5px`            | Border radius of component
`--simple-tag-input-input-min-width` | `2rem`           | Minimum width of input

To customize tag, see styling section of `simple-tag-input-tag`.

### simple-tag-input-tag

`simple-tag-input-tag` is a custom element representing a tag.

This is not only for use combined with `simple-tag-input`, but also for independent use.

#### Example

```html
<simple-tag-input-tag>lang:en</simple-tag-input-tag>
```

#### Slot

Anything can be a content of this tag (which usually is a text).

#### Properties

Name           | Default        | Description
---------------|----------------|-------------
`dismissible`  | false          | Whether tag is dismissible or not

Note that even you set `dismissible` to `true`, it's still necessary to listen `dismiss` event and delete the tag by yourself.

```js
import { SimpleTagInputTag } from "simple-tag-input";

const tag = new SimpleTagInputTag();
tag.dismissible = true;
tag.addEventListener("dismiss", ev => {
  ev.target.remove();
});

document.body.appendChild(tag);
```

#### Events

##### dismiss

Dispatched when dismiss button on tag is clicked.

#### Styling

Styles specified to `simple-tag-input-tag` by you will override default styles.

##### CSS Parts

We have `::part` ([Documentation](https://github.com/fergald/docs/blob/master/explainers/css-shadow-parts-1.md)) for custom styling.

Name      | Description
----------|-------------
`dismiss` | `button` element inside the component

##### CSS Variables

For easy customization, we provide CSS variables.

Name                                            | Default          | Description
------------------------------------------------|------------------|-------------
`--simple-tag-input-tag-background`             | `#eee`           | Background of component
`--simple-tag-input-tag-color`                  | `black`          | Text color of input
`--simple-tag-input-tag-border`                 | `1px solid #999` | Border of component
`--simple-tag-input-tag-border-radius`          | `5px`            | Border radius of component
`--simple-tag-input-tag-dismissible-background` | `transparent`    | Minimum width of input
`--simple-tag-input-tag-dismissible-color`      | `black`           | Minimum width of input

## Contribution

Thank you for your kindness!

```sh
git clone https://gitlab.com/kurousada/webcomponent-simple-tag-input
cd webcomponent-simple-tag-input
npm install
```

## License

Distributed under [MIT License](https://opensource.org/licenses/MIT).

## Contributors

 - [Kuro Usada](https://gitlab.com/kurousada)
